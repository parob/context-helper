import pytest

from werkzeug.local import LocalProxy

from context_helper.context import Context, ctx, ContextNotAvailable


class TestContext:

    def test_with_context(self):
        with pytest.raises(ContextNotAvailable):
            assert not ctx

        with Context():
            assert ctx

    def test_with_context_value(self):
        with pytest.raises(ContextNotAvailable):
            assert not ctx

        with Context(test="hello"):
            assert ctx.test == "hello"

    def test_with_context_stack(self):

        class ContextableObject:
            def __enter__(self):
                ctx.objects.append(self)
                return self

            def __exit__(self, _type, value, tb):
                ctx.objects.pop()

        object_instance = ContextableObject()

        with pytest.raises(ContextNotAvailable):
            with object_instance:
                assert True

        with Context(objects=[], object=LocalProxy(lambda: ctx.objects[-1] if len(ctx.objects) > 0 else None)):
            assert ctx
            with object_instance:
                assert object_instance == ctx.object

    def test_parent_context_clone(self):
        test_parent = "hello parent"

        with Context(parent=test_parent):
            assert ctx.parent == test_parent

            test_child = "hello child"

            with Context(child=test_child):
                assert ctx.parent == test_parent
                assert ctx.child == test_child

    def test_parent_context_no_clone(self):
        test_parent = "hello parent"

        with Context(parent=test_parent):
            assert ctx.parent == test_parent

            test_child = "hello child"

            with Context(child=test_child, clone=False):
                with pytest.raises(AttributeError):
                    assert ctx.parent

                assert ctx.child == test_child
